import {pokemonClass} from './pokemonClass';
import {showPokemon} from './showPokemon';

const displayPokemonRandom = async () => {
    const pokemonId = Math.round(Math.random() * (150 - 1) + 1);
    await fetch(`https://pokeapi.co/api/v2/pokemon/${pokemonId}/`)
    .then((response) => {
        return response.json();
    })
    .then((myJson) => {
        const data=myJson;

        const pokemon = new pokemonClass(data.name,data.height,data.id,data.sprites['other']['official-artwork']['front_default'],(data.types[0]['type']['name']));
        showPokemon(pokemon);
    })
}
export {displayPokemonRandom};