import {pokemonClass} from './pokemonClass';
import {showPokemon} from './showPokemon';

const displayPokemon = async () => {
    let pokemon = document.getElementById('inputSearchName').value;
    if (pokemon!=""){
        document.getElementById('sectionPokemon').classList.add('hidden');

        await fetch(`https://pokeapi.co/api/v2/pokemon/${pokemon}/`)

        .then((response) => {
            return response.json()
        })
        .then((myJson) => {
            const data=myJson;

            const pokemon = new pokemonClass(data.name,data.height,data.id,data.sprites['other']['official-artwork']['front_default'],(data.types[0]['type']['name']));
            showPokemon(pokemon);

            console.log(`pokemon encontrado: ${pokemon.name}`);

        })
        .catch(error => console.log("No existe pokemon"));
    }
    else{
        console.log("Introduce nombre o id del pokemon");
    }
}
export {displayPokemon};