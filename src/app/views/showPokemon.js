const showPokemon = (pokemon) =>{
    const height = document.getElementById('pokemonHeight');
    const name= document.getElementById('pokemonName');
    const id= document.getElementById('pokemonId');
    const type=document.getElementById('pokemonType');
    const img= document.getElementById('pokemonImg');

    height.innerText=`Height: ${pokemon.height} Kg`;
    name.innerText=`${pokemon.name}`;
    id.innerText=`ID: ${pokemon.id}`;
    type.innerText=`Type: ${pokemon.type}`;
    img.innerHTML=`<img src=${pokemon.img} class="pokemon-img">`;

    document.getElementById('sectionPokemon').classList.remove('hidden');
    document.getElementById('sectionPokemonList').classList.add('hidden');
    document.getElementById('mainTittle').classList.add('hidden');
    document.getElementById('mainTxtOr').classList.remove('hidden');
    document.getElementById('pokemonBtnList').classList.remove('hidden');
}
export {showPokemon};