import { displayPokemonDetails } from './view-pokemon-details';
import {pokemonClass} from './pokemonClass';

let next=null;
const fetchPokemonList = async () => {
   let url = "";
   if (next===null){ 
     url = `https://pokeapi.co/api/v2/pokemon?limit=20&offset=0`;
   }
   else{
      url=next;
   }

    const res = await fetch(url)
       .catch((error) => console.log(error)
       );

    const data  = await res.json();
    //resultado lo mete en data

    next = data.next;

    const getIdFromUrl = (url) => {
      const step1=url.split('/');
      const step2= step1.slice(-2);
      const step3= step2.join('');
      return step3;
    }

    const pokemonList = data.results.map((item) => {
        const obj = {
          name:item.name,
          id: getIdFromUrl(item.url)
        }
        return obj;
    });
      //el return es lo q devuelve el map
      return (pokemonList);
  };

const displayPokemonList = () =>{

    const pokemonListPromise = fetchPokemonList();
    pokemonListPromise.then((pokemonList) => {
        pokemonList.forEach(pokemon => {
            const li = document.createElement('li');
            let name = document.createElement('h2');
            const div = document.createElement('div');
        
            name = pokemon.name.toUpperCase();
            li.innerText=pokemon.id;
            
            pokemonMainList.appendChild(div);
            div.classList.add('main__sub-list');
            div.innerHTML=`
                <span class="sub-list__id-bg-green">${pokemon.id}</span>
                <span class="sub-list__name-bg-red">${name}</span>
                <img src="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${pokemon.id}.png" alt="">
                <button id="btnPokemonDetails${pokemon.id}" class="btn-more-details sub-list__name-bg-red">Ver detalles de ${name}</button>
            `;
            document.getElementById(`btnPokemonDetails${pokemon.id}`).onclick=function(){displayPokemonDetails(pokemon.id)};
        });

        document.getElementById('inputSearchName').value="";
        const h1=document.getElementById('mainTittleTxt');
        h1.innerText = ":: Pokemon List ::"
        document.getElementById('sectionPokemonList').classList.remove('hidden');
        document.getElementById('mainTittle').classList.remove('hidden');
        document.getElementById('sectionPokemon').classList.add('hidden');
        document.getElementById('mainTxtOr').classList.add('hidden');
        document.getElementById('pokemonBtnList').classList.add('hidden');
        if (next!='null'){
            document.getElementById('btnMasPokemon').classList.remove('hidden');
        }
        else{
            document.getElementById('btnMasPokemon').classList.add('hidden');
        }
    })
};
export {displayPokemonList};