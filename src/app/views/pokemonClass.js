class pokemonClass {
    constructor(name,height,id,img,type){
        this.name=name.toUpperCase();
        this.height=height;
        this.id=id;
        this.img=img;
        this.type=type.toUpperCase();
    }
}

export {pokemonClass};