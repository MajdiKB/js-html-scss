import { displayPokemonList } from './views/view-list';
import { displayPokemon } from './views/view-one';
import { displayPokemonRandom } from './views/view-random';

import './styles/styles.scss';
import 'bootstrap';


function addListeners() {
  document.getElementById('pokemonBtnList').addEventListener('click', displayPokemonList);
  document.getElementById('pokemonBtnRandom').addEventListener('click', displayPokemonRandom);
  document.getElementById('btnMasPokemon').addEventListener('click', displayPokemonList);
  document.getElementById('inputSearchName').addEventListener('keyup', displayPokemon);
}

window.onload = function () {
  addListeners();
};
